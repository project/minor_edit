<?php

/**
 * @file
 * Mark a node edit as minor, preventing the timestamp from updating.
 *
 * This module allows content types to be designated for minor edits, and
 * users given the permission to mark an edit.  Marking an edit as minor
 * prevents $node->changed from being updated, which keeps the node from popping
 * to the top of ordered-by-time lists.  Revisions are marked with the correct
 * timestamp data.
 *
 * Implements the 'mark minor edits' permission.  Designating allowed content
 * types uses the 'administer site configuration' permission.
 */

/**
 * Implements hook_permission().
 */
function minor_edit_permission() {
  return array(
    'mark minor edits' => array(
      'title' => t('Mark minor edits'),
      'description' => t('Prevent timestamp update on saving a node.'),
    ),
  );
}

/**
 * Implements hook_menu().
 *
 * Adds a link to the settings page in Configuration -> Content authoring.
 */
function minor_edit_menu() {
  $items = array();
  $items['admin/config/content/minor_edit/settings'] = array(
    'title' => 'Minor edit',
    'description' => 'Adjust minor edit options.',
    'page callback' => 'drupal_get_form',
    'access arguments' => array('administer site configuration'),
    'page arguments' => array('minor_edit_settings'),
    'file' => 'minor_edit.admin.inc',
    'file path' => drupal_get_path('module', 'minor_edit'),
  );
  return $items;
}

/**
 * Implements hook_form_alter().
 *
 * Adds the minor edit toggle to node-editing forms for content types on which
 * minor edits are enabled. (Node-adding forms are ignored.)
 */
function minor_edit_form_alter(&$form, $form_state, $form_id) {
  if (
    // If: $form_id identifies a node-editing form,
    preg_match('/_node_form$/', $form_id)

    // And: The content type is one configured to permit Minor Edits,
    && in_array(strstr($form_id, '_node_form', 1), variable_get('minor_edit_node_types'))

    // And: We're editing, not adding a new node.
    && arg(2) == 'edit'
    ) {

    // Define the Minor Edits fieldset.
    $form['minor'] = array(
      '#type' => 'fieldset',
      '#access' => user_access('mark minor edits'),
      '#title' => t('Minor edit toggle'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
      '#attributes' => array(
        'class' => array('node-form-minor-edit'),
      ),
      '#attached' => array(
        'js' => array(drupal_get_path('module', 'minor_edit') . '/minor_edit.js'),
      ),
      '#weight' => -10,
    );

    // Define the Minor Edits toggle checkbox.
    $form['minor']['minor_edit'] = array(
      '#access' => user_access('mark minor edits'),
      '#type' => 'checkbox',
      '#title' => t('Mark as minor edit'),
      '#default_value' => 0,
    );
  }
}

/*
 * Implements hook_node_presave().
 *
 * Prevents update of $node->changed.
 */

function minor_edit_node_presave($node) {
  unset($node->changed);
}

/**
 * Implements hook_help().
 */
function minor_edit_help($path, $arg) {
  switch ($path) {
    case 'admin/help#minor_edit':
      $output = '';
      $output .= '<h3>' . t('About Minor Edit') . '</h3>';
      $output .= '<p>' . t("Minor Edit adds a toggle to node editing forms which, if checked, prevents the node's updated time from changing.  This keeps it from floating to the top of last-updated listings.  This may be desirable behavior when the edit isn't substantial, such as typo correction.<br /><br />This behavior must be enabled per content type on the admin/config/content/minor_edit/settings page, and requires the 'mark minor edits' privilege.") . '<p>';
      return $output;
  }
}
